package br.com.bossini.pessoal_gitlab_api_livros;

import br.com.bossini.pessoal_gitlab_api_livros.model.Livro;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PessoalGitlabApiLivrosApplication {

    public static void main(String[] args) {
        SpringApplication.run(PessoalGitlabApiLivrosApplication.class, args);
    }

}
