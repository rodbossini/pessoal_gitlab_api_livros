package br.com.bossini.pessoal_gitlab_api_livros.repository;

import br.com.bossini.pessoal_gitlab_api_livros.model.Livro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LivroRepository extends JpaRepository <Livro, Long> {
}
